0\r�m��   ^   ��~N    1/0/https://openvsxorg.blob.core.windows.net/resources/usernamehw/errorlens/3.5.2/package.json{
	"name": "errorlens",
	"displayName": "Error Lens",
	"description": "Improve highlighting of errors, warnings and other language diagnostics.",
	"version": "3.5.2",
	"publisher": "usernamehw",
	"license": "MIT",
	"engines": {
		"vscode": "^1.66.0",
		"npm": ">=7.0.0"
	},
	"extensionKind": [
		"ui",
		"workspace"
	],
	"categories": [
		"Other"
	],
	"qna": false,
	"keywords": [
		"highlight",
		"problem",
		"problems",
		"error",
		"warning",
		"inline",
		"message",
		"diagnostic",
		"diagnostics",
		"status bar",
		"gutter"
	],
	"icon": "img/icon.png",
	"repository": {
		"type": "git",
		"url": "https://github.com/usernamehw/vscode-error-lens"
	},
	"activationEvents": [
		"onStartupFinished",
		"onCommand:errorLens.toggle",
		"onCommand:errorLens.toggleError",
		"onCommand:errorLens.toggleWarning",
		"onCommand:errorLens.toggleInfo",
		"onCommand:errorLens.toggleHint",
		"onCommand:errorLens.copyProblemMessage"
	],
	"main": "./dist/extension",
	"contributes": {
		"commands": [
			{
				"command": "errorLens.toggle",
				"title": "Toggle (Enable/Disable) Everything",
				"description": "Toggle (Enable/Disable) Everything (toggle global setting `errorLens.enabled`)",
				"category": "Error Lens"
			},
			{
				"command": "errorLens.toggleError",
				"title": "Toggle Errors",
				"description": "Enable/Disable Errors in `errorLens.enabledDiagnosticLevels` setting.",
				"category": "Error Lens"
			},
			{
				"command": "errorLens.toggleWarning",
				"title": "Toggle Warnings",
				"description": "Enable/Disable Warnings in `errorLens.enabledDiagnosticLevels` setting.",
				"category": "Error Lens"
			},
			{
				"command": "errorLens.toggleInfo",
				"title": "Toggle Info",
				"description": "Enable/Disable Info in `errorLens.enabledDiagnosticLevels` setting.",
				"category": "Error Lens"
			},
			{
				"command": "errorLens.toggleHint",
				"title": "Toggle Hint",
				"description": "Enable/Disable Hint in `errorLens.enabledDiagnosticLevels` setting.",
				"category": "Error Lens"
			},
			{
				"command": "errorLens.copyProblemMessage",
				"title": "Copy Problem Message",
				"description": "Copy problem message to the clipboard (at the active cursor).",
				"category": "Error Lens"
			}
		],
		"configuration": {
			"properties": {
				"errorLens.enabled": {
					"type": "boolean",
					"default": true,
					"description": "Controls all decorations and features (except commands)."
				},
				"errorLens.fontFamily": {
					"type": "string",
					"default": "",
					"description": "Font family of inline message."
				},
				"errorLens.fontWeight": {
					"type": "string",
					"enum": [
						"100",
						"200",
						"300",
						"400",
						"normal",
						"500",
						"600",
						"700",
						"bold",
						"800",
						"900"
					],
					"default": "normal",
					"markdownDescription": "Font weight of inline message. `\"normal\"` is alias for 400, `\"bold\"` is alias for 700)."
				},
				"errorLens.fontStyleItalic": {
					"type": "boolean",
					"default": false,
					"description": "When enabled - shows inline message in italic font style."
				},
				"errorLens.fontSize": {
					"type": "string",
					"default": "",
					"markdownDescription": "Font size of inline message ([CSS units](https://developer.mozilla.org/en-US/docs/Web/CSS/length))."
				},
				"errorLens.margin": {
					"type": "string",
					"default": "4ch",
					"description": "Distance between the last word on the line and the start of inline message ([CSS units](https://developer.mozilla.org/en-US/docs/Web/CSS/length))."
				},
				"errorLens.padding": {
					"type": "string",
					"default": "",
					"markdownDescription": "Padding of the inline message. Visible when `#errorLens.messageBackgroundMode#` is set to \"message\".",
					"examples": [
						"2px 1ch"
					]
				},
				"errorLens.borderRadius": {
					"type": "string",
					"default": "3px",
					"markdownDescription": "Border radius of the inline message. Visible when `#errorLens.messageBackgroundMode#` is set to \"message\".",
					"examples": [
						"5px",
						"5em 0px",
						"0px 5em 5em 0px"
					]
				},
				"errorLens.enabledDiagnosticLevels": {
					"type": "array",
					"items": {
						"type": "string",
						"enum": [
							"error",
							"warning",
							"info",
							"hint"
						]
					},
					"uniqueItems": true,
					"description": "Customize which diagnostic levels to highlight.",
					"default": [
						"error",
						"warning",
						"info"
					]
				},
				"errorLens.messageTemplate": {
					"type": "string",
					"default": "$message",
					"markdownDescription": "Template used for all inline messages. Whitespace between items is important.\n\nList of variables:\n\n- `$message` - diagnostic message text\n\n- `$count` - Number of diagnostics on the line\n\n- `$severity` - Severity prefix taken from `#errorLens.severityText#`\n\n- `$source` - Source of diagnostic e.g. \"eslint\"\n\n- `$code` - Code of the diagnostic"
				},
				"errorLens.messageMaxChars": {
					"type": "integer",
					"default": 500,
					"minimum": 10,
					"markdownDescription": "Cut off inline message if it's longer than this value. (Improves performance when the diagnostic message is long)."
				},
				"errorLens.severityText": {
					"type": "array",
					"items": {
						"type": "string"
					},
					"markdownDescription": "Replaces `$severity` variable in `#errorLens.messageTemplate#`.",
					"default": [
						"ERROR",
						"WARNING",
						"INFO",
						"HINT"
					],
					"minItems": 4,
					"maxItems": 4
				},
				"errorLens.annotationPrefix": {
					"type": "array",
					"items": {
						"type": "string"
					},
					"markdownDescription": "Specify diagnostic message prefixes (when `#errorLens.addAnnotationTextPrefixes#` is enabled). For example, emoji: ❗ ⚠ ℹ.",
					"markdownDeprecationMessage": "Deprecated in favor of `#errorLens.severityText#` & `#errorLens.messageTemplate#`. https://github.com/usernamehw/vscode-error-lens/issues/92",
					"default": [
						"ERROR: ",
						"WARNING: ",
						"INFO: ",
						"HINT: "
					],
					"maxItems": 4
				},
				"errorLens.addAnnotationTextPrefixes": {
					"type": "boolean",
					"markdownDescription": "When enabled - prepends diagnostic severity ('ERROR:', 'WARNING:' etc) to the message. (Prefixes can be configured with `annotationPrefix` setting).",
					"markdownDeprecationMessage": "Deprecated in favor of `#errorLens.messageTemplate#`. https://github.com/usernamehw/vscode-error-lens/issues/92",
					"default": false
				},
				"errorLens.addNumberOfDiagnostics": {
					"type": "boolean",
					"markdownDescription": "When enabled - prepends number of diagnostics on the line. Like: `[1/2]`.",
					"markdownDeprecationMessage": "Deprecated in favor of `#errorLens.messageTemplate#`. https://github.com/usernamehw/vscode-error-lens/issues/92",
					"default": false
				},
				"errorLens.messageEnabled": {
					"type": "boolean",
					"default": true,
					"description": "Controls whether inline message is shown or not (Including background highlight)."
				},
				"errorLens.messageBackgroundMode": {
					"type": "string",
					"default": "line",
					"enum": [
						"line",
						"message",
						"none"
					],
					"enumDescriptions": [
						"The entire line is highlighted.",
						"Inline message is highlighted.",
						"Message is not highlighted (only has foreground colors)."
					],
					"description": "Controls how inline message is highlighted in the editor (entire line / only message / none)."
				},
				"errorLens.statusBarIconsEnabled": {
					"type": "boolean",
					"markdownDescription": "When enabled - shows highlighted error/warning icons in status bar.",
					"default": false
				},
				"errorLens.statusBarIconsPriority": {
					"type": "integer",
					"markdownDescription": "Move status bar icons left or right by adjasting the number priority.",
					"default": -9000
				},
				"errorLens.statusBarIconsAlignment": {
					"type": "string",
					"markdownDescription": "Choose on which side the icons status bar is on: left or right.",
					"enum": [
						"left",
						"right"
					],
					"default": "left"
				},
				"errorLens.statusBarIconsUseBackground": {
					"type": "boolean",
					"markdownDescription": "When enabled - highlights status bar icons with background, when disabled - with foreground.",
					"default": true
				},
				"errorLens.statusBarIconsAtZero": {
					"type": "string",
					"markdownDescription": "What to do when there are 0 errors/warnings - hide the item or strip its background color.",
					"enum": [
						"hide",
						"removeBackground"
					],
					"default": "removeBackground"
				},
				"errorLens.statusBarMessageEnabled": {
					"type": "boolean",
					"markdownDescription": "When enabled - shows message in status bar.",
					"default": false
				},
				"errorLens.statusBarMessagePriority": {
					"type": "integer",
					"markdownDescription": "Move status bar message left or right by adjasting the number priority.",
					"default": -10000
				},
				"errorLens.statusBarMessageAlignment": {
					"type": "string",
					"markdownDescription": "Choose on which side the message status bar is on: left or right.",
					"enum": [
						"left",
						"right"
					],
					"default": "left"
				},
				"errorLens.statusBarColorsEnabled": {
					"type": "boolean",
					"markdownDescription": "When enabled - use message decoration foreground as color of Status Bar text.",
					"default": false
				},
				"errorLens.statusBarMessageType": {
					"type": "string",
					"markdownDescription": "Pick what to show in Status Bar: closest message or only message for the active line.",
					"enum": [
						"closestProblem",
						"closestSeverity",
						"activeLine"
					],
					"enumDescriptions": [
						"Show closest to the cursor diagnostic.",
						"Show closest to the cursor diagnostic (sorted by severity e.g. error will be shown before warning even if it's farther from the cursor).",
						"Show only diagnostic that is on the same line as the cursor."
					],
					"default": "activeLine"
				},
				"errorLens.statusBarCommand": {
					"type": "string",
					"markdownDescription": "Pick command that activates on click for Status Bar.",
					"enum": [
						"goToProblem",
						"goToLine",
						"copyMessage"
					],
					"default": "goToProblem"
				},
				"errorLens.statusBarMessageTemplate": {
					"type": "string",
					"default": "",
					"markdownDescription": "Template for status bar message. Whitespace between items is important.\n\nList of variables:\n\n- `$message` - diagnostic message text\n\n- `$count` - Number of diagnostics on the line\n\n- `$severity` - Severity prefix taken from `#errorLens.severityText#`\n\n- `$source` - Source of diagnostic e.g. \"eslint\"\n\n- `$code` - Code of the diagnostic"
				},
				"errorLens.exclude": {
					"type": "array",
					"default": [],
					"items": {
						"type": "string"
					},
					"markdownDescription": "Specify messages that should not be highlighted (RegExp). Strings passed to the RegExp constructor: `new RegExp(EXCLUDE_ITEM, 'i');`"
				},
				"errorLens.excludeBySource": {
					"type": "array",
					"default": [],
					"items": {
						"type": "string"
					},
					"markdownDescription": "Specify sources that should not be highlighted (string). Example: `[\"eslint\"]`"
				},
				"errorLens.excludePatterns": {
					"type": "array",
					"default": [],
					"items": {
						"type": "string"
					},
					"markdownDescription": "Exclude files by using glob pattern. Example `[\"**/*.{ts,js}\"]`"
				},
				"errorLens.light": {
					"type": "object",
					"description": "Specify color of decorations for when the light color theme is active.",
					"properties": {
						"errorGutterIconPath": {
							"type": "string",
							"description": "Absolute path to error gutter icon for light themes.",
							"default": ""
						},
						"warningGutterIconPath": {
							"type": "string",
							"description": "Absolute path to warning gutter icon for light themes.",
							"default": ""
						},
						"infoGutterIconPath": {
							"type": "string",
							"description": "Absolute path to info gutter icon for light themes.",
							"default": ""
						},
						"errorGutterIconColor": {
							"type": "string",
							"default": "",
							"markdownDescription": "Error color of `circle` gutter icon set for light themes.",
							"format": "color"
						},
						"warningGutterIconColor": {
							"type": "string",
							"default": "",
							"markdownDescription": "Warning color of `circle` gutter icon set for light themes.",
							"format": "color"
						},
						"infoGutterIconColor": {
							"type": "string",
							"default": "",
							"markdownDescription": "Info color of `circle` gutter icon set for light themes.",
							"format": "color"
						}
					}
				},
				"errorLens.delay": {
					"type": "integer",
					"markdownDescription": "Delay (ms) before showing problem decorations (**0** to disable). Minimum delay of **500** is enforced by the extension. New errors will be added with this delay; old errors that were fixed should disappear faster.",
					"default": 0
				},
				"errorLens.onSave": {
					"type": "boolean",
					"description": "When enabled - updates decorations only on document save (manual).",
					"default": false
				},
				"errorLens.onSaveTimeout": {
					"type": "number",
					"description": "Time period (ms) that used for showing decorations after the document save.",
					"default": 1000
				},
				"errorLens.enableOnDiffView": {
					"type": "boolean",
					"markdownDescription": "Enable decorations when viewing a diff view in the editor (e.g. Git diff).",
					"default": false
				},
				"errorLens.followCursor": {
					"type": "string",
					"enum": [
						"allLines",
						"allLinesExceptActive",
						"activeLine",
						"closestProblem"
					],
					"enumDescriptions": [
						"Highlight all problems in file",
						"Highlight all problems in file (except the line at the cursor)",
						"Highlight only the problem at the cursor",
						"Highlight only closest to the cursor problem"
					],
					"description": "Highlight only portion of the problems.",
					"default": "allLines"
				},
				"errorLens.followCursorMore": {
					"type": "number",
					"markdownDescription": "Augments `#errorLens.followCursor#`.\nAdds number of lines to top and bottom when `#errorLens.followCursor#` is set to `activeLine`.\n Adds number of closest problems when `#errorLens.followCursor#` is `closestProblem`",
					"default": 0
				},
				"errorLens.gutterIconsEnabled": {
					"type": "boolean",
					"description": "When enabled - shows gutter icons (In place of the debug breakpoint icon).",
					"default": false
				},
				"errorLens.gutterIconsFollowCursorOverride": {
					"type": "boolean",
					"markdownDescription": "When enabled and `#errorLens.followCursor#` setting is not `allLines`, then gutter icons would be rendered for all problems. But line decorations (background, message) only for active line.",
					"default": true
				},
				"errorLens.gutterIconSize": {
					"type": "string",
					"markdownDescription": "Change gutter icon size. Examples: `auto`, `contain`, `cover`, `50%`, `150%`.",
					"examples": [
						"auto",
						"contain",
						"cover",
						"50%",
						"150%"
					],
					"default": "100%"
				},
				"errorLens.gutterIconSet": {
					"type": "string",
					"description": "Change gutter icon style.",
					"enum": [
						"default",
						"defaultOutline",
						"borderless",
						"circle"
					],
					"enumDescriptions": [
						"Similar to vscode icons in Problems Panel (old).",
						"Similar to vscode icons in Problems Panel (new).",
						"Similar to vscode icons in Problems Panel, only without a border.",
						"Simple filled circle."
					],
					"default": "default"
				},
				"errorLens.errorGutterIconPath": {
					"type": "string",
					"description": "Absolute path to error gutter icon.",
					"default": ""
				},
				"errorLens.warningGutterIconPath": {
					"type": "string",
					"description": "Absolute path to warning gutter icon.",
					"default": ""
				},
				"errorLens.infoGutterIconPath": {
					"type": "string",
					"description": "Absolute path to info gutter icon.",
					"default": ""
				},
				"errorLens.errorGutterIconColor": {
					"type": "string",
					"default": "#e45454",
					"markdownDescription": "Error color of `circle` gutter icon set.",
					"format": "color"
				},
				"errorLens.warningGutterIconColor": {
					"type": "string",
					"default": "#ff942f",
					"markdownDescription": "Warning color of `circle` gutter icon set.",
					"format": "color"
				},
				"errorLens.infoGutterIconColor": {
					"type": "string",
					"default": "#00b7e4",
					"markdownDescription": "Info color of `circle` gutter icon set.",
					"format": "color"
				},
				"errorLens.removeLinebreaks": {
					"type": "boolean",
					"default": true,
					"markdownDescription": "When enabled - replaces line breaks in inline diagnostic message with whitespaces."
				},
				"errorLens.scrollbarHackEnabled": {
					"type": "boolean",
					"default": false,
					"markdownDescription": "When enabled - prevents showing horizontal scrollbar in editor (caused by inline decorations)."
				}
			}
		},
		"colors": [
			{
				"id": "errorLens.errorBackground",
				"defaults": {
					"dark": "#e454541b",
					"light": "#e4545420",
					"highContrast": "#e454541b"
				},
				"description": "Background color of the entire line containing error."
			},
			{
				"id": "errorLens.errorMessageBackground",
				"defaults": {
					"dark": "#e4545419",
					"light": "#e4545419",
					"highContrast": "#e4545419"
				},
				"description": "Background color of the error message."
			},
			{
				"id": "errorLens.errorBackgroundLight",
				"defaults": {
					"dark": "#e4545420",
					"light": "#e4545420",
					"highContrast": "#e4545420"
				},
				"description": "Background color of the entire line containing error (Only in light themes)."
			},
			{
				"id": "errorLens.errorForeground",
				"defaults": {
					"dark": "#ff6464",
					"light": "#e45454",
					"highContrast": "#ff6464"
				},
				"description": "Text color used to highlight lines containing errors."
			},
			{
				"id": "errorLens.errorForegroundLight",
				"defaults": {
					"dark": "#e45454",
					"light": "#e45454",
					"highContrast": "#e45454"
				},
				"description": "Text color used to highlight lines containing errors (Only in light themes)."
			},
			{
				"id": "errorLens.warningBackground",
				"defaults": {
					"dark": "#ff942f1b",
					"light": "#ff942f20",
					"highContrast": "#ff942f1b"
				},
				"description": "Background color used to highlight lines containing warnings."
			},
			{
				"id": "errorLens.warningMessageBackground",
				"defaults": {
					"dark": "#ff942f19",
					"light": "#ff942f19",
					"highContrast": "#ff942f19"
				},
				"description": "Background color of the warning message."
			},
			{
				"id": "errorLens.warningBackgroundLight",
				"defaults": {
					"dark": "#ff942f20",
					"light": "#ff942f20",
					"highContrast": "#ff942f20"
				},
				"description": "Background color used to highlight lines containing warnings (Only in light themes)."
			},
			{
				"id": "errorLens.warningForeground",
				"defaults": {
					"dark": "#fa973a",
					"light": "#ff942f",
					"highContrast": "#fa973a"
				},
				"description": "Text color used to highlight lines containing warnings."
			},
			{
				"id": "errorLens.warningForegroundLight",
				"defaults": {
					"dark": "#ff942f",
					"light": "#ff942f",
					"highContrast": "#ff942f"
				},
				"description": "Text color used to highlight lines containing warnings (Only in light themes)."
			},
			{
				"id": "errorLens.infoBackground",
				"defaults": {
					"dark": "#00b7e420",
					"light": "#00b7e420",
					"highContrast": "#00b7e420"
				},
				"description": "Background color used to highlight lines containing info."
			},
			{
				"id": "errorLens.infoMessageBackground",
				"defaults": {
					"dark": "#00b7e419",
					"light": "#00b7e419",
					"highContrast": "#00b7e419"
				},
				"description": "Background color of the info message."
			},
			{
				"id": "errorLens.infoBackgroundLight",
				"defaults": {
					"dark": "#00b7e420",
					"light": "#00b7e420",
					"highContrast": "#00b7e420"
				},
				"description": "Background color used to highlight lines containing info (Only in light themes)."
			},
			{
				"id": "errorLens.infoForeground",
				"defaults": {
					"dark": "#00b7e4",
					"light": "#00b7e4",
					"highContrast": "#00b7e4"
				},
				"description": "Text color used to highlight lines containing info."
			},
			{
				"id": "errorLens.infoForegroundLight",
				"defaults": {
					"dark": "#00b7e4",
					"light": "#00b7e4",
					"highContrast": "#00b7e4"
				},
				"description": "Text color used to highlight lines containing info (Only in light themes)."
			},
			{
				"id": "errorLens.hintBackground",
				"defaults": {
					"dark": "#17a2a220",
					"light": "#17a2a220",
					"highContrast": "#17a2a220"
				},
				"description": "Background color used to highlight lines containing hints."
			},
			{
				"id": "errorLens.hintMessageBackground",
				"defaults": {
					"dark": "#17a2a219",
					"light": "#17a2a219",
					"highContrast": "#17a2a219"
				},
				"description": "Background color of the hint message."
			},
			{
				"id": "errorLens.hintBackgroundLight",
				"defaults": {
					"dark": "#17a2a220",
					"light": "#17a2a220",
					"highContrast": "#17a2a220"
				},
				"description": "Background color used to highlight lines containing hints (Only in light themes)."
			},
			{
				"id": "errorLens.hintForeground",
				"defaults": {
					"dark": "#2faf64",
					"light": "#2faf64",
					"highContrast": "#2faf64"
				},
				"description": "Text color used to highlight lines containing hints."
			},
			{
				"id": "errorLens.hintForegroundLight",
				"defaults": {
					"dark": "#2faf64",
					"light": "#2faf64",
					"highContrast": "#2faf64"
				},
				"description": "Text color used to highlight lines containing hints (Only in light themes)."
			},
			{
				"id": "errorLens.statusBarIconErrorForeground",
				"defaults": {
					"dark": "#ff6464",
					"light": "#e45454",
					"highContrast": "#ff6464"
				},
				"description": "Status bar icon item error color. Foreground is used when the `errorLens.statusBarIconsUseBackground` setting is disabled."
			},
			{
				"id": "errorLens.statusBarIconWarningForeground",
				"defaults": {
					"dark": "#fa973a",
					"light": "#ff942f",
					"highContrast": "#fa973a"
				},
				"description": "Status bar icon item error color. Foreground is used when the `errorLens.statusBarIconsUseBackground` setting is disabled."
			},
			{
				"id": "errorLens.statusBarErrorForeground",
				"defaults": {
					"dark": "#ff6464",
					"light": "#e45454",
					"highContrast": "#ff6464"
				},
				"description": "Status bar item error color."
			},
			{
				"id": "errorLens.statusBarWarningForeground",
				"defaults": {
					"dark": "#fa973a",
					"light": "#ff942f",
					"highContrast": "#fa973a"
				},
				"description": "Status bar item warning color."
			},
			{
				"id": "errorLens.statusBarInfoForeground",
				"defaults": {
					"dark": "#00b7e4",
					"light": "#00b7e4",
					"highContrast": "#00b7e4"
				},
				"description": "Status bar item info color."
			},
			{
				"id": "errorLens.statusBarHintForeground",
				"defaults": {
					"dark": "#2faf64",
					"light": "#2faf64",
					"highContrast": "#2faf64"
				},
				"description": "Status bar item hint color."
			}
		]
	},
	"scripts": {
		"vscode:prepublish": "webpack --mode production --color",
		"watch": "webpack --mode development --watch --stats minimal"
	},
	"devDependencies": {
		"@soda/friendly-errors-webpack-plugin": "^1.8.1",
		"@types/lodash": "^4.14.182",
		"@types/node": "16.x",
		"@types/vscode": "^1.66.0",
		"@typescript-eslint/eslint-plugin": "^5.30.6",
		"@typescript-eslint/parser": "^5.30.6",
		"eslint": "^8.19.0",
		"eslint-plugin-no-autofix": "1.2.3",
		"ts-loader": "^9.3.1",
		"typescript": "^4.7.4",
		"webpack": "^5.73.0",
		"webpack-cli": "^4.10.0"
	},
	"dependencies": {
		"lodash": "^4.17.21"
	}
}
�A�Eo��   j�937_        E�(��B/ �9��B/ �  HTTP/1.1 200 OK Cache-Control: max-age=2592000, public Content-Length: 24375 Content-Type: application/json Last-Modified: Wed, 13 Jul 2022 14:15:52 GMT ETag: 0x8DA64DA3194C326 Server: Windows-Azure-Blob/1.0 Microsoft-HTTPAPI/2.0 x-ms-request-id: 24b03836-201e-000a-793c-99e046000000 x-ms-version: 2009-09-19 x-ms-lease-status: unlocked x-ms-blob-type: BlockBlob Access-Control-Allow-Origin: * Date: Sat, 16 Jul 2022 17:51:35 GMT      8  0�40�� -;|B���R���   -;|0	*�H�� 0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 010220615233715Z230615233715Z0"1 0U*.blob.core.windows.net0�"0	*�H�� � 0�
� ��CK��9���3T�J��hr�/��]�y28K�x�l���X���z��m��#���(iwD����ʞ5ؑ�gMh�EK���~â�;��jcqS�>,��L�]�7��9zg��l'��d�=<�8�B!8�%�|]�R�x%^���αَ�D��L���=06���;/��&���u��G�2㨀�T��W�ͧ��.n�	�xG�rը�φ��Lg��L�!��f��C�̚�u���!r�gp ���� ��
40�
00�~
+�y�n�jh v ����|�ȋ�=�>j�g)]ϱ$ʅ�4�܂�  �ié�   G0E! ����;)͎�E�b���oEw0������_��Q w���YmO��H��z��^Z�T#lC��p�� v z2�Tط-� �8�R�p2�M;�+�:W�R�R  �ié�   G0E! �~��!���m����p�H^��1_����w,� '�����W���r�\��!W���1BE>� v �>��>�52�W(��k����k��i�w}m�n  �ié�   G0E! ��*3e�* Z����5����]���S� v�,�6�~��lW�#�����ցyư��88GF�0'	+�7
00
+0
+0>	+�710/'+�7�چu����Ʌ���a���`�]���A�Pd'0��+{0y0S+0�Ghttp://www.microsoft.com/pki/mscorp/Microsoft%20RSA%20TLS%20CA%2001.crt0"+0�http://ocsp.msocsp.com0U`��M��WX �N��#��ܾ0U��0�<U�30�/�*.blob.core.windows.net�'*.dub09prdstr08a.store.core.windows.net�*.blob.storage.azure.net�*.z1.blob.storage.azure.net�*.z2.blob.storage.azure.net�*.z3.blob.storage.azure.net�*.z4.blob.storage.azure.net�*.z5.blob.storage.azure.net�*.z6.blob.storage.azure.net�*.z7.blob.storage.azure.net�*.z8.blob.storage.azure.net�*.z9.blob.storage.azure.net�*.z10.blob.storage.azure.net�*.z11.blob.storage.azure.net�*.z12.blob.storage.azure.net�*.z13.blob.storage.azure.net�*.z14.blob.storage.azure.net�*.z15.blob.storage.azure.net�*.z16.blob.storage.azure.net�*.z17.blob.storage.azure.net�*.z18.blob.storage.azure.net�*.z19.blob.storage.azure.net�*.z20.blob.storage.azure.net�*.z21.blob.storage.azure.net�*.z22.blob.storage.azure.net�*.z23.blob.storage.azure.net�*.z24.blob.storage.azure.net�*.z25.blob.storage.azure.net�*.z26.blob.storage.azure.net�*.z27.blob.storage.azure.net�*.z28.blob.storage.azure.net�*.z29.blob.storage.azure.net�*.z30.blob.storage.azure.net�*.z31.blob.storage.azure.net�*.z32.blob.storage.azure.net�*.z33.blob.storage.azure.net�*.z34.blob.storage.azure.net�*.z35.blob.storage.azure.net�*.z36.blob.storage.azure.net�*.z37.blob.storage.azure.net�*.z38.blob.storage.azure.net�*.z39.blob.storage.azure.net�*.z40.blob.storage.azure.net�*.z41.blob.storage.azure.net�*.z42.blob.storage.azure.net�*.z43.blob.storage.azure.net�*.z44.blob.storage.azure.net�*.z45.blob.storage.azure.net�*.z46.blob.storage.azure.net�*.z47.blob.storage.azure.net�*.z48.blob.storage.azure.net�*.z49.blob.storage.azure.net�*.z50.blob.storage.azure.net0��U��0��0���������Mhttp://mscrl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2001.crl�Khttp://crl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2001.crl0WU P0N0B	+�7*0503+'http://www.microsoft.com/pki/mscorp/cps0g�0U#0��v0�ǒBML�\,ȩ�d0U%0++0	*�H�� �  �	��C4ko�bP\�x���d���(������W�u�����uԑt��A�z{Q�|�wkl��
:V�5+)�
��J�~$���ݳ��?	���`6�@t>��4t���i���ר�.��	c,>��fÁ�S�e�bT�:y򢸷��'P����s��RU�AD�}�}��&k$��iD���7�-1	����77{�94��E.��h�Gl���̴���B���r� ����5g��������sP���B�Zf�H��#�͆d�P�D8>�~F3�3�`�W���Ǣ5�e�����'V���mU����A�j�K&�f:�-�80���2;�6�)�+�=Hf�'���v�/̧)����q7�ew�8�&�����׹	��[ȼK�s�ؿ��S{_`���@��X �)L(��[n��~�M}�%뺄)H����؊����O��v�V֘��^u���P�r��Xe@j�@[J��y��겊��j@E��i�6�����n�b^  0�Z0�B��_  i�O�Ǭx�A�0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0200721230000Z241008070000Z0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 010�"0	*�H�� � 0�
� �bwϚc���6���Q��
;F�A�[X�8n�ODH�:����╸�Q ſ�pc]xC�)�dI	�گ��_+*��n.�,��i��?S�\�l�"g��,Ҿ[c7̯`��kk��n� �k�p�����1g34�%�b[X�uv���?;ߪB��2m��C&���:�,Ȋ��?���%������J��뀔���#I礻Ǒ��y4?AJ�:&��oF�8s��ԑ�x�x���s���e��O\�R�����M	��q�Rz�;כT�b�Z'�0&��MV�z{���S1��.�At����E��1�FB16C��_������T���djӜ��y4�=s�6�h�Yp��|��nE�6�C&w�p�WTDB���c&��#�|oT�+N]Ο�>Ҙ[����0RH8c���#�>��'��Bsם#hq�Y^R��M�|Not"�S��`N�1O�NHc��������iL�K����:��0�J`��dOCD�� ��%0�!0U�v0�ǒBML�\,ȩ�d0U#0��Y0�GX̬�T6�{:�M�0U��0U%0++0U�0� 04+(0&0$+0�http://ocsp.digicert.com0:U3010/�-�+�)http://crl3.digicert.com/Omniroot2025.crl0*U #0!0g�0g�0	+�7*0	*�H�� � �+��g[�{�����M��A���`��"V�H����+<���к�����|cq��z��0F��}���"��6�o¿.n��v(��>����f@Q�羿Ng��TYcoB�1`d &�ф��z;p�E&ܖF=�f?�:�8�6(W��Kd��m`��z�����?10��dמ����Vn8�4?��lkBRb<�i��WY���r$B���c�4�v7��>pa��Go㊈0+GE^yT�����?xQu�[  {  0�w0�_�  �0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0000512184600Z250512235900Z0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0�"0	*�H�� � 0�
� ��"��=W�&r��y�)��蕀���[�+)�dߡ]��	m�(.�b�b�����8�!��A+R{�w��Ǻ���j	�s�@����b�-��PҨP�(���%�����g�?���R/��pp����˚���3zw����hDBH��¤�^`������Y�Y�c��c��}]�z�����^�>_��i��96ru�wRM�ɐ,�=��#S?$�!\�)��:��n�:k�tc3�h1�x�v����]*��M��'9 �E0C0U�Y0�GX̬�T6�{:�M�0U�0�0U�0	*�H�� � �]��oQhB�ݻO'%���d�-�0���))�y?v�#�
�X��ap�aj��
�ż0|��%��@O�̣~8�7O��h1�Lҳt�u^Hp�\��y����e����R79թ1z��*����E��<^����Ȟ|.Ȥ�NKm�pmkc�d�����.���P�s������2�����~5���>0�z�3=�e����b�GD,]��2�G�8.����2j��<����$B�c9�     0�P 
   20.60.40.4  �          ���o+�j�򯝈��MVA�Ӌ���2
r�A�Eo��   ���      