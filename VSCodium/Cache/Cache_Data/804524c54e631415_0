0\r�m��   a   �E�    1/0/https://openvsxorg.blob.core.windows.net/resources/Darkempire78/discord-tools/1.4.2/README.md![CodeFactor](https://img.shields.io/codefactor/grade/github/Darkempire78/Discord-Tools?style=for-the-badge)
![Visual Studio Marketplace](https://img.shields.io/visual-studio-marketplace/v/Darkempire78.discord-tools?style=for-the-badge)
<a href="https://marketplace.visualstudio.com/items?itemName=Darkempire78.discord-tools"><img src="https://img.shields.io/visual-studio-marketplace/i/Darkempire78.discord-tools?style=for-the-badge"></a> <a href="https://discord.com/invite/sPvJmY7mcV"><img src="https://img.shields.io/discord/831524351311609907?color=%237289DA&label=DISCORD&style=for-the-badge"></a>

# Discord Tools

Discord Tools is a Visual Studio Code extension to code Discord bots more easily.

- **GITHUB :** https://github.com/Darkempire78/Discord-Tools
- **DOWNLOAD :** https://marketplace.visualstudio.com/items?itemName=Darkempire78.discord-tools

## Supported Languages

- [x] Javascript ([**Discord.js**](https://discord.js.org/#/), [**Eris**](https://abal.moe/Eris/))
- [x] Typescript ([**Harmony**](https://harmony.mod.land/))
- [x] Python ([**Discord.py**](https://discordpy.readthedocs.io/en/latest/))
- [x] Java ([**JDA**](https://github.com/DV8FromTheWorld/JDA))
- [ ] Soon...

## Index
- [**Discord chat in VSCode**](##beta-discord-chat-in-vscode-)
- [**Generate a template Discord bot**](#generate-a-template-discord-bot-)
- [**Open the Discord bot Documentation with/without a research**](#open-the-discord-bot-documentation-withwithout-a-research-)
- [**Javascript snippets**](#available-snippets)
  - [Discord.js](#javascript-discordjs-)
  - [Eris](#javascript-eris-)
- [**Typescript snippets**](#typescript-harmony-)
  - [Harmony](#typescript-harmony-)
-  [**Python snippets**](#python-discordpy-)
    - [Discord.py](#python-discordpy-)
-  [**Java snippets**](#java-jda-)
    - [JDA](#java-jda-)
- [**Themes**](#discord-theme-dark-version)
- [**Functionality table**](#functionality-table)
- [**Contributing**](#contributing)
- [**Release notes**](#release-notes)
- [**License**](#license)

## Features

### [BETA] Discord chat in VSCode : 
⚠️ **This feature is currently in BETA, so it is possible that some features are not yet implemented and you may find bugs. If you find an error, thank you for reporting it [here](https://github.com/Darkempire78/Discord-Tools/issues/new)**

⚠️ It seems that the way this extension works is not entirely within Discord's terms of service. See the [Discord's terms](https://discord.com/terms) for more.
However, please note that this extension does not collect personal information and does not automatically respond to messages!

#### Instalation
You must set up your personal Discord token with the command: `Discord Chat: Set up your personal Discord toke` (`Crtl + Maj + P` to open the Command palette)
You can find your Discord token with the command: `Discord Chat: Grab your personal Discord Token`. However, if this does not work, you can follow [this tutorial](https://www.youtube.com/watch?v=YEgFvgg7ZPI)

#### Features
* Discord Chat
* Navigation tree through the servers (DM will be supported in the future)
* Parameters

#### Commands
* `Discord Chat: Set up your personal Discord token`
* `Discord Chat: Start the Discord chat`
* `Discord Chat: Grab your personal Discord Token`
* `Discord Chat: Set up if the Discord chat should start when VSCode is opened`
* `Discord Chat: Open the Discord Chat (must be started before)`
* `Discord Chat: Reload the Discord Bot`

#### Preview
<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/Capture2.png" width="700"/>

#### To do (soon)
- [ ] Fix small chat glitches
- [ ] Refresh the Navigation tree when the chat is reload
- [ ] Embed support
- [ ] File support
- [ ] Private message support

### Generate a template Discord bot : 
- Open the command palette (Ctrl+Shift+P) and choose : `Generate a <language> template bot (Discord.<language>)` (not available for JDA, Harmony and Eris)

⚠️ A folder must be opened in VSCode to execute this command

<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/video1.gif" width="500"/>


### Open the Discord bot Documentation with/without a research :
- Open the command palette (Ctrl+Shift+P) and choose : `Open the Discord bot Documention`
- Shortcut : `Ctrl+Alt+D`
- Works with Discord.py, Discord.js and JDA (JDA, Harmony and Eris documentions do not allow to make a research)

⚠️ A folder must be opened in VSCode to execute this command

<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/video5.gif" width="500"/>

### Generate code easily :

### Available Snippets

#### Javascript ([Discord.js](https://discord.js.org/#/)) :

- `djs.index` : Create a basic Discord bot index.js file.
- `djs.cmd` : Create a basic Discord command.
- `djs.cmd+` : Create a complex Discord command.
- `djs.embed` : Create a basic Discord embed.
- `djs.embed+` : Create a complex Discord embed.
- `djs.message` : Create a default Discord bot message event.
- `djs.guildmemberadd` : Create a default Discord bot guildMemberAdd event.
- `djs.guildmemberremove` : Create a default Discord bot guildMemberRemove event.
- `djs.guildcreate` : Create a default Discord bot guildCreate event.
- `djs.guilddelete` : Create a default Discord bot guildDelete event.
- **and 47 other events...**

#### Javascript ([Eris](https://abal.moe/Eris/)) :
- `eris.index` : Create a basic Discord bot index.js file.
- `eris.cmd` : Create a basic Discord command.
- `eris.cmd+` : Create a complex Discord command.
- `eris.embed` : Create a basic Discord embed.
- `eris.embed+` : Create a complex Discord embed.
- `eris.message` : Create a default Discord bot message event.

#### Javascript Preview :
<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/video2.gif" width="500"/>

#### Typescript ([Harmony](https://harmony.mod.land/)) :
- `hy.index` : Create a basic Discord bot index.ts file.
- `hy.cmd` : Create a basic Discord command.
- `hy.cmd+` : Create a complex Discord command.
- `hy.embed` : Create a basic Discord embed.
- `hy.embed+` : Create a complex Discord embed.
- `hy.message` : Create a default Discord bot message event.

#### Python ([Discord.py](https://discordpy.readthedocs.io/en/latest/)) :

- `dpy.main` : Create a basic Discord bot main.py file.
- `dpy.cog` : Create a basic Discord cog.
- `dpy.cmd` : Create a basic Discord command.
- `dpy.cmd+` : Create a complex Discord command.
- `dpy.embed` : Create a basic Discord embed.
- `dpy.embed+` : Create a complex Discord embed.
- `dpy.onmessage` : CCreate a default Discord bot on_message event.
- `dpy.onmemberjoin` : Create a default Discord bot on_member_join event.
- `dpy.onmemberremove` : Create a default Discord bot on_member_remove event.
- `dpy.onguildjoin` : Create a default Discord bot on_guild_join event.
- `dpy.onguildremove` : Create a default Discord bot on_guild_remove event.
- **and 59 other events...**

#### Python Preview :
<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/video3.gif" width="500"/>

#### Java ([JDA](https://github.com/DV8FromTheWorld/JDA)) :

- `jda.main` : Create a basic Discord bot main function.
- `jda.cmd` : Create a basic Discord command.
- `jda.embed` : Create a basic Discord embed.
- `jda.embed+` : Create a complex Discord embed.
- `jda.onmessagereceived` : Create a default Discord bot on.

#### Java Preview :
<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/video4.gif" width="500"/>

### Discord Theme (Dark version)
Work for Python and JavasScript
Activate the theme : `CTRL + K + CTRL + T` and choose `Discord Theme (Dark)`

#### Preview :
<img src="https://github.com/Darkempire78/Discord-Tools/raw/master/images/capture1.png" width="700"/>

## Functionality table

|            | Template | Doc | Doc searcher | Snippets |
|------------|:--------:|:---:|:------------:|:--------:|
| Discord.js |     ✅    |  ✅  |       ✅      |     ✅    |
| Eris       |     ❌    |  ❌  |       ❌      |     ✅    |
| Harmony    |     ❌    |  ✅  |       ❌      |     ✅    |
| Discord.py |     ✅    |  ✅  |       ✅      |     ✅    |
| JDA        |     ❌    |  ✅  |       ❌      |     ✅    |

## Discord

Join the Discord server !

[![](https://i.imgur.com/UfyvtOL.png)](https://discord.gg/sPvJmY7mcV)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


## Release Notes

### 1.4.2
- The servers in the Discord chat navigation tree are collapsed by default
- Custom emojis support
### 1.4.1 (Latest update)
- Small fix
### 1.4.0 
- Discord chat [BETA]
- JSON Discord Theme fixed 
- Log system
### 1.3.3 
- New Discord Theme (Javascript, Python)
### 1.3.2
- Update of the Discord.js snippets
- Update of the Discord.js template
- Fix an issue with the discord-tools.openDiscordDoc command 
- Several small corrections
### 1.3.1 
- Update of the Discord.js template
- Update of the Discord.py template
### 1.3.0 
- Add the Harmony support
### 1.2.2
- Fix the python template
### 1.2.1
- Fix workspace error
- Small corrections
### 1.2.0 
- Add the Eris support
- Add the definition of python parameters
- Fix small python snippets
### 1.1.6 
- Several python snippet corrections
- Correction of the python template 
### 1.1.5 
- New snippet aliases
- New snippet choices
- Several corrections
### 1.1.4
- New command : open the Discord bot Documentation with/without a research (works with Discord.py, Discord.js, JDA)
### 1.1.3 
- New Discord.js bot template
- New bot template generator system
- New auto package downloader
- Few small updates
### 1.1.2
- 48 new Discord.js events added
- Addition of capital letters to Discord.py events
- Few small updates
### 1.1.1 
- Change of snippet prefixes to `dpy` (Discord.py), `djs` (Discord.js) and `jda` (JDA)
- 59 new Discord.py events added
### 1.1.0
- New language supported : Java (JDA)
### 1.0.1
- Several corrections
### 1.0.0
- Initial release


## License

This project is under [GPLv3](https://github.com/Darkempire78/Raid-Protect-Discord-Bot/blob/master/LICENSE).
�A�Eo��   �[E��(        E�S�A/ �s�A/ �  HTTP/1.1 200 OK Cache-Control: max-age=2592000 Content-Length: 10449 Content-Type: text/plain Last-Modified: Sat, 05 Jun 2021 15:31:21 GMT ETag: 0x8D92836F8800153 Server: Windows-Azure-Blob/1.0 Microsoft-HTTPAPI/2.0 x-ms-request-id: 7dfb6e7d-e01e-003a-58f6-8e5e89000000 x-ms-version: 2009-09-19 x-ms-lease-status: unlocked x-ms-blob-type: BlockBlob Access-Control-Allow-Origin: * Date: Sun, 03 Jul 2022 16:02:49 GMT        8  0�40�� -;|B���R���   -;|0	*�H�� 0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 010220615233715Z230615233715Z0"1 0U*.blob.core.windows.net0�"0	*�H�� � 0�
� ��CK��9���3T�J��hr�/��]�y28K�x�l���X���z��m��#���(iwD����ʞ5ؑ�gMh�EK���~â�;��jcqS�>,��L�]�7��9zg��l'��d�=<�8�B!8�%�|]�R�x%^���αَ�D��L���=06���;/��&���u��G�2㨀�T��W�ͧ��.n�	�xG�rը�φ��Lg��L�!��f��C�̚�u���!r�gp ���� ��
40�
00�~
+�y�n�jh v ����|�ȋ�=�>j�g)]ϱ$ʅ�4�܂�  �ié�   G0E! ����;)͎�E�b���oEw0������_��Q w���YmO��H��z��^Z�T#lC��p�� v z2�Tط-� �8�R�p2�M;�+�:W�R�R  �ié�   G0E! �~��!���m����p�H^��1_����w,� '�����W���r�\��!W���1BE>� v �>��>�52�W(��k����k��i�w}m�n  �ié�   G0E! ��*3e�* Z����5����]���S� v�,�6�~��lW�#�����ցyư��88GF�0'	+�7
00
+0
+0>	+�710/'+�7�چu����Ʌ���a���`�]���A�Pd'0��+{0y0S+0�Ghttp://www.microsoft.com/pki/mscorp/Microsoft%20RSA%20TLS%20CA%2001.crt0"+0�http://ocsp.msocsp.com0U`��M��WX �N��#��ܾ0U��0�<U�30�/�*.blob.core.windows.net�'*.dub09prdstr08a.store.core.windows.net�*.blob.storage.azure.net�*.z1.blob.storage.azure.net�*.z2.blob.storage.azure.net�*.z3.blob.storage.azure.net�*.z4.blob.storage.azure.net�*.z5.blob.storage.azure.net�*.z6.blob.storage.azure.net�*.z7.blob.storage.azure.net�*.z8.blob.storage.azure.net�*.z9.blob.storage.azure.net�*.z10.blob.storage.azure.net�*.z11.blob.storage.azure.net�*.z12.blob.storage.azure.net�*.z13.blob.storage.azure.net�*.z14.blob.storage.azure.net�*.z15.blob.storage.azure.net�*.z16.blob.storage.azure.net�*.z17.blob.storage.azure.net�*.z18.blob.storage.azure.net�*.z19.blob.storage.azure.net�*.z20.blob.storage.azure.net�*.z21.blob.storage.azure.net�*.z22.blob.storage.azure.net�*.z23.blob.storage.azure.net�*.z24.blob.storage.azure.net�*.z25.blob.storage.azure.net�*.z26.blob.storage.azure.net�*.z27.blob.storage.azure.net�*.z28.blob.storage.azure.net�*.z29.blob.storage.azure.net�*.z30.blob.storage.azure.net�*.z31.blob.storage.azure.net�*.z32.blob.storage.azure.net�*.z33.blob.storage.azure.net�*.z34.blob.storage.azure.net�*.z35.blob.storage.azure.net�*.z36.blob.storage.azure.net�*.z37.blob.storage.azure.net�*.z38.blob.storage.azure.net�*.z39.blob.storage.azure.net�*.z40.blob.storage.azure.net�*.z41.blob.storage.azure.net�*.z42.blob.storage.azure.net�*.z43.blob.storage.azure.net�*.z44.blob.storage.azure.net�*.z45.blob.storage.azure.net�*.z46.blob.storage.azure.net�*.z47.blob.storage.azure.net�*.z48.blob.storage.azure.net�*.z49.blob.storage.azure.net�*.z50.blob.storage.azure.net0��U��0��0���������Mhttp://mscrl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2001.crl�Khttp://crl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2001.crl0WU P0N0B	+�7*0503+'http://www.microsoft.com/pki/mscorp/cps0g�0U#0��v0�ǒBML�\,ȩ�d0U%0++0	*�H�� �  �	��C4ko�bP\�x���d���(������W�u�����uԑt��A�z{Q�|�wkl��
:V�5+)�
��J�~$���ݳ��?	���`6�@t>��4t���i���ר�.��	c,>��fÁ�S�e�bT�:y򢸷��'P����s��RU�AD�}�}��&k$��iD���7�-1	����77{�94��E.��h�Gl���̴���B���r� ����5g��������sP���B�Zf�H��#�͆d�P�D8>�~F3�3�`�W���Ǣ5�e�����'V���mU����A�j�K&�f:�-�80���2;�6�)�+�=Hf�'���v�/̧)����q7�ew�8�&�����׹	��[ȼK�s�ؿ��S{_`���@��X �)L(��[n��~�M}�%뺄)H����؊����O��v�V֘��^u���P�r��Xe@j�@[J��y��겊��j@E��i�6�����n�b^  0�Z0�B��_  i�O�Ǭx�A�0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0200721230000Z241008070000Z0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 010�"0	*�H�� � 0�
� �bwϚc���6���Q��
;F�A�[X�8n�ODH�:����╸�Q ſ�pc]xC�)�dI	�گ��_+*��n.�,��i��?S�\�l�"g��,Ҿ[c7̯`��kk��n� �k�p�����1g34�%�b[X�uv���?;ߪB��2m��C&���:�,Ȋ��?���%������J��뀔���#I礻Ǒ��y4?AJ�:&��oF�8s��ԑ�x�x���s���e��O\�R�����M	��q�Rz�;כT�b�Z'�0&��MV�z{���S1��.�At����E��1�FB16C��_������T���djӜ��y4�=s�6�h�Yp��|��nE�6�C&w�p�WTDB���c&��#�|oT�+N]Ο�>Ҙ[����0RH8c���#�>��'��Bsם#hq�Y^R��M�|Not"�S��`N�1O�NHc��������iL�K����:��0�J`��dOCD�� ��%0�!0U�v0�ǒBML�\,ȩ�d0U#0��Y0�GX̬�T6�{:�M�0U��0U%0++0U�0� 04+(0&0$+0�http://ocsp.digicert.com0:U3010/�-�+�)http://crl3.digicert.com/Omniroot2025.crl0*U #0!0g�0g�0	+�7*0	*�H�� � �+��g[�{�����M��A���`��"V�H����+<���к�����|cq��z��0F��}���"��6�o¿.n��v(��>����f@Q�羿Ng��TYcoB�1`d &�ф��z;p�E&ܖF=�f?�:�8�6(W��Kd��m`��z�����?10��dמ����Vn8�4?��lkBRb<�i��WY���r$B���c�4�v7��>pa��Go㊈0+GE^yT�����?xQu�[  {  0�w0�_�  �0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0000512184600Z250512235900Z0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0�"0	*�H�� � 0�
� ��"��=W�&r��y�)��蕀���[�+)�dߡ]��	m�(.�b�b�����8�!��A+R{�w��Ǻ���j	�s�@����b�-��PҨP�(���%�����g�?���R/��pp����˚���3zw����hDBH��¤�^`������Y�Y�c��c��}]�z�����^�>_��i��96ru�wRM�ɐ,�=��#S?$�!\�)��:��n�:k�tc3�h1�x�v����]*��M��'9 �E0C0U�Y0�GX̬�T6�{:�M�0U�0�0U�0	*�H�� � �]��oQhB�ݻO'%���d�-�0���))�y?v�#�
�X��ap�aj��
�ż0|��%��@O�̣~8�7O��h1�Lҳt�u^Hp�\��y����e����R79թ1z��*����E��<^����Ȟ|.Ȥ�NKm�pmkc�d�����.���P�s������2�����~5���>0�z�3=�e����b�GD,]��2�G�8.����2j��<����$B�c9�     0�P 
   20.60.40.4  �          V^�CId���u�g����0Fˏ�/'��l��o�A�Eo��   4��x      