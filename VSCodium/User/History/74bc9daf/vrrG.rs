use crate::{HeadTile, Direction, Position, BodyTile, Apple};

pub struct Snake {
    head: HeadTile,
    body: Vec<BodyTile>,
    current_direction: Direction,
}

impl Snake {
    pub fn new(&size: i16, head_position: Position) -> &mut Snake {
        let snake = Snake {
        head: HeadTile {
            position: Position {
                x: WIDTH / 2,
                y: HEIGTH / 2,
            },
            previous_position: Position {
                x: WIDTH / 2,
                y: HEIGTH / 2,
            },
        },
        body: Vec::new(),
        current_direction: previous_direction,
    };

    snake.body().push(BodyTile {
        position: Position {
            x: WIDTH / 2,
            y: (HEIGTH / 2) + 1,
        },
        previous_position: Position {
            x: WIDTH / 2,
            y: (HEIGTH / 2) + 1,
        },
    });
    }

    pub fn body(&self) -> &Vec<BodyTile> {
        &self.body
    }

    pub fn is_snakes_body(&self, position: &Position) -> bool {
        for tile in &self.body {
            if &tile.position == position {
                return true;
            }
        }
        false
    }

    pub fn is_snakes_head(&self, position: &Position) -> bool {
        &self.head.position == position
    }

    pub fn add_body_tile(&mut self) {
        self.body.push(BodyTile {
            position: self.body[self.body.len() - 1].previous_position,
            previous_position: self.body[self.body.len() - 1].previous_position,
        });
    }

    pub fn move_snake(&mut self, direction: &Direction) {
        self.head.previous_position = self.head.position;

        if direction == &Direction::Up {
            self.head.position.y -= 1;
        } else if direction == &Direction::Down {
            self.head.position.y += 1;
        } else if direction == &Direction::Left {
            self.head.position.x -= 1;
        } else if direction == &Direction::Right {
            self.head.position.x += 1;
        }
        
        self.body[0].previous_position = self.body[0].position;
        self.body[0].position = self.head.previous_position;

        for i in 1..self.body.len() {
            self.body[i].previous_position = self.body[i].position;
            self.body[i].position = self.body[i-1].previous_position;
        }
    }

    pub fn continue_move_snake(&mut self) {
        self.head.previous_position = self.head.position;

        if self.current_direction == Direction::Up {
            self.head.position.y -= 1;
        } else if self.current_direction == Direction::Down {
            self.head.position.y += 1;
        } else if self.current_direction == Direction::Left {
            self.head.position.x -= 1;
        } else if self.current_direction == Direction::Right {
            self.head.position.x += 1;
        }
        
        self.body[0].previous_position = self.body[0].position;
        self.body[0].position = self.head.previous_position;

        for i in 1..self.body.len() {
            self.body[i].previous_position = self.body[i].position;
            self.body[i].position = self.body[i-1].previous_position;
        }
    }
    pub fn is_dead(&self) -> bool {
        let mut collided: bool = false;
        for tile in &self.body {
            collided = self.head.position == tile.position;
            if collided {break;}
        }
        self.head.position.is_in_border() || collided
    }
    
    pub fn is_apple(&self, apple: &Apple) -> bool {
        apple.position == self.head.position
    }

    fn is_rotation_posible(&self, rotation_direction: Direction) -> bool {
        if rotation_direction == Direction::Down {
            return self.body[0].position.y == self.head.position.y + 1;
        } else if rotation_direction == Direction::Up {
            return self.body[0].position.y == self.head.position.y - 1;
        } else if rotation_direction == Direction::Left {
            return self.body[0].position.x == self.head.position.x - 1;
        } else if rotation_direction == Direction::Right {
            return self.body[0].position.x == self.head.position.x + 1;
        }
        true
    }
}