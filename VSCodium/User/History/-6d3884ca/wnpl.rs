use bevy::{prelude::*, sprite::collide_aabb::Collision};

pub const CLEAR: Color = Color::rgb(0.1, 0.1, 0.1);

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .run();
}
