use bevy::{prelude::*, render::{camera::ScalingMode, render_resource::Texture}, ecs::system::Command};

pub const CLEAR: Color = Color::rgb(0.1, 0.1, 0.1);

fn main() {
    App::new()
        .insert_resource(ClearColor(CLEAR))
        .insert_resource(WindowDescriptor {
            width: 900.0,
            height: 900.0,
            title: String::from("Snake Game"),
            ..Default::default()

        })
        .add_startup_system(spawn_camera)
        .add_plugins(DefaultPlugins)
        .run();
}

fn spawn_camera(mut commands: Commands) {
    let mut camera = OrthographicCameraBundle::new_2d();
    camera.orthographic_projection.top = 1.0;
    camera.orthographic_projection.bottom = -1.0;
    camera.orthographic_projection.right = 1.0;
    camera.orthographic_projection.left = -1.0;
    
    camera.orthographic_projection.scaling_mode = ScalingMode::None;

    commands.spawn_bundle(camera);
}

fn load_textures(
    mut commands: Command,
    assets: Res<AssetServer>,
    mut aylasses: ResMut<Assets<Texture>>) {
        
    }
