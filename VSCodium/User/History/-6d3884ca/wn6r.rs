use bevy::prelude::*;

pub const CLEAR: Color = Color::rgb(0.1, 0.1, 0.1);

fn main() {
    App::new()
        .insert_resource(ClearColor(CLEAR))
        .insert_resource(WindowDescriptor {
            width: 20.0,
            height: 20.0,
            title: "Snake game",
            position: todo!(),
            resize_constraints: todo!(),
            scale_factor_override: todo!(),
            present_mode: todo!(),
            resizable: todo!(),
            decorations: todo!(),
            cursor_visible: todo!(),
            cursor_locked: todo!(),
            mode: todo!(),
            transparent: todo!(),
        })
        .add_plugins(DefaultPlugins)
        .run();
}
