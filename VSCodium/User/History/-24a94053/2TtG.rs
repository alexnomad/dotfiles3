use rand::Rng;

use crate::WIDTH;

#[derive(Copy, Clone, PartialEq)]
pub struct Position {
    pub x: i16,
    pub y: i16,
}

impl Position {
    pub fn is_in_border(&self) -> bool {
        self.x > 20 || self.y > 20 || self.x < 0 || self.y < 0
    }
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{0};{1}", self.x, self.y)
    }
}

pub fn random_position() -> Position {
    let mut rng = rand::thread_rng();
    let x: i16 = rng.gen_range(0..WIDTH);
    let y: i16 = rng.gen_range(0..WIDTH);
    Position { x, y }
}

#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl std::fmt::Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}