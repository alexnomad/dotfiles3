use crate::{Position, snake::Snake, util::random_position};

pub struct Apple {
    pub position: Position,
}

impl Apple {
    pub fn new(snake: &Snake) -> Apple {
        let mut position: Position = random_position();
        while snake.is_snakes_body(&position) {
            position = random_position();
        }
        Apple { position }
    }
}