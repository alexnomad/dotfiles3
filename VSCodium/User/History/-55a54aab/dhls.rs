use crate::{Position, snake::Snake, random_position};


pub struct Apple {
    position: Position,
}

impl Apple {
    pub fn position(&self) -> &Position {
        &self.position
    }

    pub fn new(snake: &Snake) -> Apple {
        let mut position: Position = random_position();
        while snake.is_snakes_body(&position) {
            position = random_position();
        }
        Apple { position }
    }
}