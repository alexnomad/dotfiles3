mod snake;


use rand::Rng;
use snake::Snake;
use tokio::time::{Duration, self};

static WIDTH: i16 = 20;
static HEIGTH: i16 = 20;
static DEBUG: bool = true;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let mut snake = Snake::new(Position {
        x: WIDTH / 2,
        y: HEIGTH /2,
    });

    let apple = Apple::new(&snake);

    // End Of Setup

    let tick_duration: Duration = Duration::from_millis(500);
    let mut tick_interval = time::interval(tick_duration);


    loop {
        tick_interval.tick().await;
        snake = tick_task(snake, Direction::Up).await;
        if snake.is_dead() {
            println!("GAME OVER SCORE: {}", snake.body().len());
            break;
        }
        if snake.is_apple(&apple) {
            snake.add_body_tile();
        }
        draw_task(&snake, &apple).await;
    }
    Ok(())
}

async fn tick_task(mut snake: Snake, direction: Direction) -> Snake {

    snake.move_snake(&direction);
    // snake.add_body_tile();
    snake
}

async fn draw_task(snake: &Snake, apple: &Apple) {
    print!("{}[2J", 27 as char); // Magical symbol that clears the terminal
    
    let width: usize = WIDTH as usize;

    if DEBUG {
        println!("Snake's head is at {}", snake.head().position);
        for tile in snake.body() {
            println!("Snake's body tile is at {}", tile.position);
        }
    }
    let heigth: usize = HEIGTH as usize;
    let mut grid: Vec<Vec<char>> = vec![vec![Default::default(); width]; heigth];
    for x in 0..WIDTH {
        for y in 0..HEIGTH {
            if snake.is_snakes_body(&Position {x, y}) {
                grid[y as usize][x as usize] = '🟨';
            } else if snake.is_snakes_head(&Position {x, y}) {
                grid[y as usize][x as usize] = '🟧';
            }
             else if apple.position == (Position {x, y}) {
                grid[y as usize][x as usize] = '🟥';
            } else {
                grid[y as usize][x as usize] = '⬛';
            }
        }
    }

    for rows in grid {
        for c in rows {
            print!("{}", c);
        }
        println!();
    }
}



pub struct Apple {
    position: Position,
}

impl Apple {
    pub fn new(snake: &Snake) -> Apple {
        let mut position: Position = random_position();
        while snake.is_snakes_body(&position) {
            position = random_position();
        }
        Apple { position }
    }
}

pub struct HeadTile {
    position: Position,
    previous_position: Position,
}

pub struct BodyTile {
    position: Position,
    previous_position: Position,
}

#[derive(Copy, Clone, PartialEq)]
pub struct Position {
    x: i16,
    y: i16,
}

impl Position {
    fn is_in_border(&self) -> bool {
        self.x > 20 || self.y > 20 || self.x < 0 || self.y < 0
    }
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{0};{1}", self.x, self.y)
    }
}

fn random_position() -> Position {
    let mut rng = rand::thread_rng();
    let x: i16 = rng.gen_range(0..WIDTH);
    let y: i16 = rng.gen_range(0..WIDTH);
    Position { x, y }
}

#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl std::fmt::Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}