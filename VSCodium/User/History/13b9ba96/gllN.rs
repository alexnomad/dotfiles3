mod snake;
mod apple;
mod util;

use rand::Rng;
use snake::Snake;
use apple::Apple;
use tokio::time::{Duration, self};

static WIDTH: i16 = 20;
static HEIGTH: i16 = 20;
static DEBUG: bool = true;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let mut snake = Snake::new(Position {
        x: WIDTH / 2,
        y: HEIGTH /2,
    });

    let apple = Apple::new(&snake);

    // End Of Setup

    let tick_duration: Duration = Duration::from_millis(500);
    let mut tick_interval = time::interval(tick_duration);


    loop {
        tick_interval.tick().await;
        snake = tick_task(snake, Direction::Up).await;
        if snake.is_dead() {
            println!("GAME OVER SCORE: {}", snake.body().len());
            break;
        }
        if snake.is_apple(&apple) {
            snake.add_body_tile();
        }
        draw_task(&snake, &apple).await;
    }
    Ok(())
}

async fn tick_task(mut snake: Snake, direction: Direction) -> Snake {

    snake.move_snake(&direction);
    // snake.add_body_tile();
    snake
}

async fn draw_task(snake: &Snake, apple: &Apple) {
    print!("{}[2J", 27 as char); // Magical symbol that clears the terminal
    
    let width: usize = WIDTH as usize;

    if DEBUG {
        println!("Snake's head is at {}", snake.head().position);
        for tile in snake.body() {
            println!("Snake's body tile is at {}", tile.position);
        }
    }
    let heigth: usize = HEIGTH as usize;
    let mut grid: Vec<Vec<char>> = vec![vec![Default::default(); width]; heigth];
    for x in 0..WIDTH {
        for y in 0..HEIGTH {
            if snake.is_snakes_body(&Position {x, y}) {
                grid[y as usize][x as usize] = '🟨';
            } else if snake.is_snakes_head(&Position {x, y}) {
                grid[y as usize][x as usize] = '🟧';
            }
             else if apple.position == (Position {x, y}) {
                grid[y as usize][x as usize] = '🟥';
            } else {
                grid[y as usize][x as usize] = '⬛';
            }
        }
    }

    for rows in grid {
        for c in rows {
            print!("{}", c);
        }
        println!();
    }
}



