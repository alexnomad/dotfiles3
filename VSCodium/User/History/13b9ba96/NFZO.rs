mod snake;
mod apple;
mod util;
mod theme;

use snake::Snake;
use apple::Apple;
use tokio::time::{Duration, self};
use util::{Position, Direction};
use theme::*;

static WIDTH: i16 = 20;
static HEIGHT: i16 = 20;
static DEBUG: bool = true;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    // Setup
    let mut snake = Snake::new(Position {
        x: WIDTH / 2,
        y: HEIGHT /2,
    });

    let apple = Apple::new(&snake);

    // Game loop
    let tick_duration: Duration = Duration::from_millis(500);
    let mut tick_interval = time::interval(tick_duration);

    loop {
        tick_interval.tick().await;
        snake = tick_task(snake, Direction::Up).await;
        if snake.is_dead() {
            println!("GAME OVER SCORE: {}", snake.body().len());
            break;
        }
        if snake.is_apple(&apple) {
            snake.add_body_tile();
        }
        draw_task(&snake, &apple).await;
    }
    Ok(())
}

async fn tick_task(mut snake: Snake, direction: Direction) -> Snake {
    snake.move_snake(&direction);
    snake
}

async fn draw_task(snake: &Snake, apple: &Apple) {
    print!("{}[2J", 27 as char); // Clearing Terminal

    if DEBUG {
        println!("Snake's head is at {}", snake.head().position);
        for tile in snake.body() {
            println!("Snake's body tile is at {}", tile.position);
        }
    }
    
    let width: usize = WIDTH as usize;
    let height: usize = HEIGHT as usize;
    

    // Theming 
    let t = Theme::load();

    // Grid is used only for rendereing purposes
    let mut grid: Vec<Vec<char>> = vec![vec![Default::default(); width]; height];
    for x in 0..WIDTH {
        for y in 0..HEIGHT {
            if snake.is_snakes_body(&Position {x, y}) {
                grid[y as usize][x as usize] = t.body();
            } else if snake.is_snakes_head(&Position {x, y}) {
                grid[y as usize][x as usize] = '🟧';
            }
             else if apple.position == (Position {x, y}) {
                grid[y as usize][x as usize] = '🟥';
            } else {
                grid[y as usize][x as usize] = '⬛';
            }
        }
    }

    for rows in grid {
        for c in rows {
            print!("{}", c);
        }
        println!();
    }
}
