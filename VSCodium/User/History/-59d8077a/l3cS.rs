use std::fs::File;


pub struct Theme {
    background: char,
    head: char,
    body: char,
    apple: char,
}

impl Theme {
    pub fn default() -> Theme {
        Theme {
            background: '⬛',
            head: '🟧',
            body: '🟨',
            apple: '🟥',
        }
    }

    pub fn load() -> Theme {
        let mut file = File::open("theme.yaml");
        let mut contents
    }
}