use std::{fs::{File, OpenOptions}, io::Read};
use serde::{Serialize, Deserialize, __private::de::StrDeserializer};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Theme {
    background: char,
    head: char,
    body: char,
    apple: char,
}

impl Theme {
    pub fn default() -> Theme {
        Theme {
            background: '⬛',
            head: '🟧',
            body: '🟨',
            apple: '🟥',
        }
    }

    pub fn load() -> Theme {
        let mut file = OpenOptions::new().read(true).open("theme.yaml");
        match file {
            Ok(f) => {
                let mut contents = String::new();
                f.read_to_string(contents);

                let deserialization_ressult = deserialize_theme(&contents);

                match deserialization_ressult {
                    Ok(theme) => theme,
                    Err(_) => todo!(),
                }
            },
            Err(_) => {Theme::default()},
        }
    }
}

fn deserialize_theme(string: &String) -> Result<Theme, serde_yaml::Error> {
    let deserialized_theme: Theme = serde_yaml::from_str(&string)?;
    Ok(deserialized_theme)
}