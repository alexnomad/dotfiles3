use std::{fs::{File, OpenOptions}, io::Read};


pub struct Theme {
    background: char,
    head: char,
    body: char,
    apple: char,
}

impl Theme {
    pub fn default() -> Theme {
        Theme {
            background: '⬛',
            head: '🟧',
            body: '🟨',
            apple: '🟥',
        }
    }

    pub fn load() -> Theme {
        let mut file = OpenOptions::new().read(true).open("theme.yaml");
        match file {
            Ok(f) => {
                f.read_to_string()
            },
            Err(_) => todo!(),
        }
    }
}